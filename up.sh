#!/bin/bash
echo ""
echo "Docker registry"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $REGISTRY_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $REGISTRY_CONTAINER_VOLUME_AUTH
    docker volume create --name $REGISTRY_CONTAINER_VOLUME_DATA
    
    echo ""
    echo "Create password file ..."
    SOURCE_BIND=$PWD/.auth.temp
    DEST_VOLUME=$REGISTRY_CONTAINER_VOLUME_AUTH
    rm -rf $SOURCE_BIND
    mkdir $SOURCE_BIND

    echo "docker run --rm --name ${REGISTRY_CONTAINER_NAME}_passwd --entrypoint htpasswd $REGISTRY_CONTAINER_IMAGE -Bbn xxx yyy > $SOURCE_BIND/htpasswd"
    docker run --rm --name ${REGISTRY_CONTAINER_NAME}_passwd --entrypoint htpasswd $REGISTRY_CONTAINER_IMAGE -Bbn $REGISTRY_USER $REGISTRY_PASSWORD > $SOURCE_BIND/htpasswd
    
    echo "Copy $SOURCE_BIND/htpasswd to $DEST_VOLUME..."
    docker run --rm -v $SOURCE_BIND:/source -v $DEST_VOLUME:/dest -w /source alpine cp /source/htpasswd /dest/htpasswd
    rm -rf $SOURCE_BIND

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

